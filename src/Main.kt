import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.onClickFunction
import objJSON.loads
import org.w3c.dom.*

import kotlin.browser.document

/**
 * Created by ian on 25/06/2017.
 */
fun dummyul(outer: DIV, jsondata:Map<*,*>){

    val coded = jsondata["subtitle"]  // val coded = jsondata.
    outer.h2{ +"a ${coded}"}
}
class Calc(baseElt: Element, data:Map<*,*>){
    val accsave: HTMLInputElement
    var progsave: HTMLInputElement
    var pcbutton: HTMLButtonElement
    var bregbutton: HTMLButtonElement
    var opregbutton: HTMLButtonElement
    var pc = 0
    var breg = 0
    var displayOpen = false
    init{
        val div = document.create.div {
            h1{
                +data["title"].toString()
            }
            dummyul(this, data)
            a ("https://kotlinfrompython.wordpress.com/2017/06/27/machine-code-and-global-memory/") {
                +"(See this page for instructions)"
                target ="_blank"
            }
            br()
            button{ +"Op Reg";id="opreg"};br()
            button{ +"PC: $pc";id="PC"};br{}
            button{ +"B: $breg";id="breg"};br{}
            div{
                textInput{
                    width = "20"
                    id = "acc"
                }
                +"Accumulator"
            }
            div{
                textInput{
                    id = "code"
                    value = ""
                }
                +"Code"
            }

            button{ +"Reset"; onClickFunction = { doReset()}}
            button{ +"Step"; onClickFunction = { doStep()}}
            button{ +" + "; onClickFunction = { doPlus()}}
            button{ +" = "; onClickFunction = { doEq()}}
            br()
            for(but in 1..9){
                button{
                    +"$but"
                    onClickFunction = {
                        println("click $but test")
                        doButton(but)
                    }

                }
                if(but %3 == 0) br()
            }
            button{
                +"<"
                onClickFunction = {backButton()}
            }
            button{
                +"0"
                onClickFunction = {doButton(0)}
            }
            button{
                +"C"
                onClickFunction = {clearButton()}
            }
        }

        //println(div.)


        baseElt.appendChild(div)
        progsave = document.getElementById("code")!! as HTMLInputElement
        pcbutton = document.getElementById("PC")!! as HTMLButtonElement
        bregbutton = document.getElementById("breg")!! as HTMLButtonElement
        opregbutton = document.getElementById("opreg")!! as HTMLButtonElement
        accsave = document.getElementById("acc")!! as HTMLInputElement
        println("2sav $pcbutton")
    }

    fun doButton(butval: Int){
            if(!displayOpen){
                displayOpen = true
                accsave.value = ""
            }
            accsave.value += butval.toString()
    }
    fun backButton(){
        val len = accsave.value.length
        if(len >0)
            accsave.value = accsave.value.substring(0..len-2)
    }
    fun clearButton(){
        accsave.value = ""
    }
    fun doReset(){
        pc = 0
        pcbutton.textContent = "PC: $pc"
    }
    fun getInstruct():Int{
        val nums = progsave.value.split(' ').map{it.toIntOrNull()?: 0}
        val res = if(pc < nums.size) nums[pc++] else 0
        //pc += 1
        return res
    }
    fun doStep(){
        when(getInstruct()){
            0 -> { // stop!
            }
            1 -> {  doPlus() //breg = accsave.value.toIntOrNull() ?: 0
//                    bregbutton.textContent = "B: $breg"
//                    opregbutton.textContent = "OPREG: 1"
            }
            2 -> { doEq()

            }
            3 -> { accsave.value = getInstruct().toString()
            }
            4 -> { // load acc
            }
            5 -> { // store acc
            }
        }
        pcbutton.textContent = "PC: $pc"


    }
    fun doPlus(){
        breg = accsave.value.toIntOrNull() ?: 0
        bregbutton.textContent = "B: $breg"
        opregbutton.textContent = "OPREG: 1"
        displayOpen = false

    }
    fun doEq(){
        accsave.value = ((accsave.value.toIntOrNull() ?: 0) + breg).toString()
        displayOpen = false
    }

}
fun String.jsonloads() = loads(this)
fun main(args: Array<String>){
    val root: Element = document.getElementById("root")!!

    val jsdataelt = document.getElementById ("jsondata") as HTMLScriptElement?

    val jsdata = jsdataelt?.text?.jsonloads() ?: "".jsonloads()
    println("loads done")
    val jsmap = if (jsdata is Map<*,*>)jsdata else mapOf("none" to 0)
    val pageTitle = document.getElementsByTagName("title").get(0) as HTMLTitleElement?
    pageTitle?.text = jsmap["title"].toString()

//    val jsdata = jsdataelt?.let{
//        println("got it$jsdataelt  ${jsdataelt.value}")
//        loads(jsdataelt.value)
//
//        } ?: {loads("")}
    val calc = Calc(root, jsmap)

}