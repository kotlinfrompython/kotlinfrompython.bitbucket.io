/**
 * Created by ian on 19/05/2017.
 */
package objJSON

import kotlin.coroutines.experimental.*
//  { "abc":5, { "def": "sss" }
fun iterMarks(strng: String, specials:List<String>, idx:Int=0)=buildSequence{
    val inquotes = listOf("\\","\"")
    var found:Pair<Int,String>?
    //var infound:Pair<Int,String>?
    var index = idx
    do{
        found = strng.findAnyOf(specials, index)
        if (found?.second == "\""){
            index = found.first
            inner@ do{
                found = strng.findAnyOf(inquotes, index + 1)
                when(found?.second){
                    "\"" -> {index = found.first+1; break@inner}
                    "\\" -> index = found.first+1
                }
            }while (found != null)
        }
        else if(found != null){
            yield(found)
            index = found.first + 1
        }
    }while (found != null)
}
//
//>>> for c in inputs('     " , , { }',0):
//print(c)
typealias Holder = MutableMap<String,Any?>

class Parser(val strng:String){
    inline fun whiteSpace(ch: Char):Boolean {
        return " \t\n".contains(ch)
    }
    fun consume(start:Int):Int{
        /**  this method skips over a , that can follow closing braces */
        val res = strng.findAnyOf(listOf(",", "}", "]" ),start+1)
        return if(res?.second == ",") res.first+1 else start+1
    }
    fun stripQuotes(fullstrng: String):String{
        if(fullstrng =="") return ""
        val strng = fullstrng.dropWhile{whiteSpace(it)}
        val start = if(strng[0]=='"') 1 else 0
        val end = if(strng.last()=='"') 2 else 1
        return strng.slice(start..strng.length-end)
    }
    fun toType(strng:String):Any{
        val ival = strng.toIntOrNull()
        ival?.let { return ival}
        return stripQuotes(strng)
    }
    fun Holder.adder(adStr:String,obj:Any?=null){
        val key = stripQuotes(adStr.trim().split(":")[0])
        when{

            key == "" -> {}
            obj == null ->{
                this[key] = if(":" in adStr)
                    toType(adStr.trim().split(":",limit=2)[1])
                else null
            }
            else -> { this[key] = obj }
        }
    }
    fun getStart():Int{
        // skip leaing whitespace, and first '{' character
        var count = 0
        while((count< strng.length) && whiteSpace(strng[count]))
            count++
        if(strng[count]== '{') count++
        return count
    }
    fun parse(idxIn:Int=-1): Pair<Any,Int> {
        val specials = listOf("\"", "{", "}", ",","[")
        var hold:Holder = mutableMapOf()
        var idx = if(idxIn < 0) getStart() else idxIn
        loop@ for (mark in iterMarks(strng, specials, idx)){
            if(mark==null){

            }else if(mark.first <= idx){
                // nothing needed
            }
            else
            {
                when( mark.second ) {
                    "}" -> {
                        if(mark.first-1 > idx)
                            hold.adder(strng.slice(idx..mark.first - 1))
                        idx = consume(mark.first)
                        break@loop
                    }
                    "[" -> {
                        val res: Pair<Any, Int>
                        res = parseLst(mark.first + 1)
                        hold.adder(strng.slice(idx..mark.first - 1),res.first)
                        idx = res.second
                    }
                    "{" -> {
                        val res: Pair<Any, Int>
                        res = parse(mark.first + 1)
                        hold.adder(strng.slice(idx..mark.first - 1),res.first)
                        idx = res.second
                    }

                    "," -> {
                        hold.adder(strng.slice(idx..mark.first - 1))
                        idx = mark.first + 1
                    }
                }

            }
        }
        return Pair(hold, idx)

    }
    fun MutableList<Any>.adder(adStr:String,obj:Any?=null){
        //val key = stripQuotes(adStr.trim().split(":")[0])
        if(obj == null){
            this.add(toType(adStr))
        }
        else{
            this.add(obj)
        }
    }
    fun parseLst(idxIn:Int=1): Pair<Any,Int> {
        val specials = listOf("\"", "{", "]", ",","[")
        var lst = mutableListOf<Any>()
        var idx = idxIn
        loop@ for (mark in iterMarks(strng, specials, idx)){
            if(mark==null){

            }else if(mark.first <= idx){
                // nothing needed
            }
            else
            {
                when( mark.second ) {
                    "]" -> {
                        lst.adder(strng.slice(idx..mark.first - 1))
                        idx = consume(mark.first)
                        break@loop
                    }
                    "[" -> {
                        val res: Pair<Any, Int>
                        res = parseLst(mark.first + 1)
                        lst.adder(strng.slice(idx..mark.first - 1),res.first)
                        idx = res.second
                    }
                    "{" -> {
                        val res: Pair<Any, Int>
                        res = parse(mark.first + 1)
                        lst.adder(strng.slice(idx..mark.first - 1),res.first)
                        idx = res.second
                    }

                    "," -> {
                        lst.adder(strng.slice(idx..mark.first - 1))
                        idx = mark.first + 1
                    }
                }

            }
        }
        return Pair(lst, idx)

    }
}


fun loads(arg: String):Any{

    //for( )
    //dostring()
    return Parser(arg).parse().first
}
fun String.jsonLoads() = loads(this)

class RawStr(val str:String){
    fun res():String{ return str}
}

// MyClass::class.declaredMemberProperties

class Dumper(val dataAll: Any){
    fun quoter(strng:String):String{
        val res = "\"" + strng + "\""
        return res
    }
    fun dump(data: Any):String{
        when (data){
            is Int-> return data.toString()
            is String -> return quoter(data)
            is Map<*,*> -> return dumpmap(data)
            is List<*> -> return dumplst(data)
            is RawStr -> return data.res()
            }
        return "UnKnown"
        }
    fun dumpmap(mapData:Map<*,*>):String{
        //var answer:StringBuilder = StringBuilder()
        val lstStr = mapData.toList().map{
            val key = it.first
            val dat:Any? = it.second
            val strdat = if(dat==null) "null" else dump(dat)
            "\"$key\": ${strdat}"
            }
        val redStr = lstStr.reduce{v1,v2-> v1 + ", " + v2}
        return "{ $redStr }"
    }
    fun dumplst(lstData:List<*>):String{
        //var answer:StringBuilder = StringBuilder()
        val lstStr = lstData.map{
            val dat:Any? = it
            //val strdat =
            if(dat==null) "null" else dump(dat)
        }
        val redStr = lstStr.reduce{v1,v2-> v1 + ", " + v2}
        return "[ $redStr ]"
    }
    fun dumps():String{
        return dump(dataAll)
    }
}
fun dumps(arg:Any):String{
    return Dumper(arg).dumps()
}