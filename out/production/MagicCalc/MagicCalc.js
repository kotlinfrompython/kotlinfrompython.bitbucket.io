if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'MagicCalc'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'MagicCalc'.");
}
if (typeof this['kotlinx-html-js'] === 'undefined') {
  throw new Error("Error loading module 'MagicCalc'. Its dependency 'kotlinx-html-js' was not found. Please, check whether 'kotlinx-html-js' is loaded prior to 'MagicCalc'.");
}
var MagicCalc = function (_, Kotlin, $module$kotlinx_html_js) {
  'use strict';
  var h2 = $module$kotlinx_html_js.kotlinx.html.h2_eh5gi3$;
  var IntRange = Kotlin.kotlin.ranges.IntRange;
  var substring = Kotlin.kotlin.text.substring_fc3b62$;
  var split = Kotlin.kotlin.text.split_o64adg$;
  var toIntOrNull = Kotlin.kotlin.text.toIntOrNull_pdl1vz$;
  var get_create = $module$kotlinx_html_js.kotlinx.html.dom.get_create_4wc2mh$;
  var h1 = $module$kotlinx_html_js.kotlinx.html.h1_vmej1w$;
  var a = $module$kotlinx_html_js.kotlinx.html.a_gu26kr$;
  var br = $module$kotlinx_html_js.kotlinx.html.br_5bz84p$;
  var set_id = $module$kotlinx_html_js.kotlinx.html.set_id_ueiko3$;
  var button = $module$kotlinx_html_js.kotlinx.html.button_whohl6$;
  var textInput = $module$kotlinx_html_js.kotlinx.html.textInput_ap9uf6$;
  var div = $module$kotlinx_html_js.kotlinx.html.div_ri36nr$;
  var set_onClickFunction = $module$kotlinx_html_js.kotlinx.html.js.set_onClickFunction_pszlq2$;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var div_0 = $module$kotlinx_html_js.kotlinx.html.div_59el9d$;
  var Map = Kotlin.kotlin.collections.Map;
  var to = Kotlin.kotlin.to_ujzrz7$;
  var mapOf = Kotlin.kotlin.collections.mapOf_x2b85n$;
  var listOf = Kotlin.kotlin.collections.listOf_i5x0yv$;
  var findAnyOf = Kotlin.kotlin.text.findAnyOf_7utkvz$;
  var CoroutineImpl = Kotlin.kotlin.coroutines.experimental.CoroutineImpl;
  var intrinsics = Kotlin.kotlin.coroutines.experimental.intrinsics;
  var buildSequence = Kotlin.kotlin.coroutines.experimental.buildSequence_of7nec$;
  var last = Kotlin.kotlin.text.last_gw00vp$;
  var slice = Kotlin.kotlin.text.slice_fc3b62$;
  var split_0 = Kotlin.kotlin.text.split_ip8yn$;
  var contains = Kotlin.kotlin.text.contains_li3zpu$;
  var Pair = Kotlin.kotlin.Pair;
  var List = Kotlin.kotlin.collections.List;
  var toList = Kotlin.kotlin.collections.toList_abgq59$;
  function dummyul$lambda(closure$coded) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('a ' + Kotlin.toString(closure$coded));
    };
  }
  function dummyul(outer, jsondata) {
    var key = 'subtitle';
    var tmp$;
    var coded = (Kotlin.isType(tmp$ = jsondata, Kotlin.kotlin.collections.Map) ? tmp$ : Kotlin.throwCCE()).get_11rb$(key);
    h2(outer, void 0, dummyul$lambda(coded));
  }
  function Calc(baseElt, data) {
    this.accsave = null;
    this.progsave = null;
    this.pcbutton = null;
    this.bregbutton = null;
    this.opregbutton = null;
    this.pc = 0;
    this.breg = 0;
    this.displayOpen = false;
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8;
    var div = div_0(get_create(document), void 0, Calc_init$lambda(data, this));
    baseElt.appendChild(div);
    this.progsave = Kotlin.isType(tmp$_0 = (tmp$ = document.getElementById('code')) != null ? tmp$ : Kotlin.throwNPE(), HTMLInputElement) ? tmp$_0 : Kotlin.throwCCE();
    this.pcbutton = Kotlin.isType(tmp$_2 = (tmp$_1 = document.getElementById('PC')) != null ? tmp$_1 : Kotlin.throwNPE(), HTMLButtonElement) ? tmp$_2 : Kotlin.throwCCE();
    this.bregbutton = Kotlin.isType(tmp$_4 = (tmp$_3 = document.getElementById('breg')) != null ? tmp$_3 : Kotlin.throwNPE(), HTMLButtonElement) ? tmp$_4 : Kotlin.throwCCE();
    this.opregbutton = Kotlin.isType(tmp$_6 = (tmp$_5 = document.getElementById('opreg')) != null ? tmp$_5 : Kotlin.throwNPE(), HTMLButtonElement) ? tmp$_6 : Kotlin.throwCCE();
    this.accsave = Kotlin.isType(tmp$_8 = (tmp$_7 = document.getElementById('acc')) != null ? tmp$_7 : Kotlin.throwNPE(), HTMLInputElement) ? tmp$_8 : Kotlin.throwCCE();
    println('2sav ' + this.pcbutton);
  }
  Calc.prototype.doButton_za3lpa$ = function (butval) {
    if (!this.displayOpen) {
      this.displayOpen = true;
      this.accsave.value = '';
    }
    this.accsave.value = this.accsave.value + butval.toString();
  };
  Calc.prototype.backButton = function () {
    var len = this.accsave.value.length;
    if (len > 0)
      this.accsave.value = substring(this.accsave.value, new IntRange(0, len - 2 | 0));
  };
  Calc.prototype.clearButton = function () {
    this.accsave.value = '';
  };
  Calc.prototype.doReset = function () {
    this.pc = 0;
    this.pcbutton.textContent = 'PC: ' + this.pc;
  };
  Calc.prototype.getInstruct = function () {
    var tmp$, tmp$_0;
    var $receiver = split(this.progsave.value, [32]);
    var destination = Kotlin.kotlin.collections.ArrayList_init_ww73n8$(Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$($receiver, 10));
    var tmp$_1;
    tmp$_1 = $receiver.iterator();
    while (tmp$_1.hasNext()) {
      var item = tmp$_1.next();
      var tmp$_2;
      destination.add_11rb$((tmp$_2 = toIntOrNull(item)) != null ? tmp$_2 : 0);
    }
    var nums = destination;
    if (this.pc < nums.size) {
      tmp$_0 = nums.get_za3lpa$((tmp$ = this.pc, this.pc = tmp$ + 1 | 0, tmp$));
    }
     else
      tmp$_0 = 0;
    var res = tmp$_0;
    return res;
  };
  Calc.prototype.doStep = function () {
    var tmp$;
    tmp$ = this.getInstruct();
    if (tmp$ !== 0)
      if (tmp$ === 1)
        this.doPlus();
      else if (tmp$ === 2)
        this.doEq();
      else if (tmp$ === 3)
        this.accsave.value = this.getInstruct().toString();
    this.pcbutton.textContent = 'PC: ' + this.pc;
  };
  Calc.prototype.doPlus = function () {
    var tmp$;
    this.breg = (tmp$ = toIntOrNull(this.accsave.value)) != null ? tmp$ : 0;
    this.bregbutton.textContent = 'B: ' + this.breg;
    this.opregbutton.textContent = 'OPREG: 1';
    this.displayOpen = false;
  };
  Calc.prototype.doEq = function () {
    var tmp$;
    this.accsave.value = (((tmp$ = toIntOrNull(this.accsave.value)) != null ? tmp$ : 0) + this.breg | 0).toString();
    this.displayOpen = false;
  };
  function Calc_init$lambda$lambda(closure$data) {
    return function ($receiver) {
      var $receiver_0 = closure$data;
      var tmp$;
      $receiver.unaryPlus_pdl1vz$(Kotlin.toString((Kotlin.isType(tmp$ = $receiver_0, Kotlin.kotlin.collections.Map) ? tmp$ : Kotlin.throwCCE()).get_11rb$('title')));
    };
  }
  function Calc_init$lambda$lambda_0($receiver) {
    $receiver.unaryPlus_pdl1vz$('(See this page for instructions)');
    $receiver.target = '_blank';
  }
  function Calc_init$lambda$lambda_1($receiver) {
    $receiver.unaryPlus_pdl1vz$('Op Reg');
    set_id($receiver, 'opreg');
  }
  function Calc_init$lambda$lambda_2(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('PC: ' + this$Calc.pc);
      set_id($receiver, 'PC');
    };
  }
  function Calc_init$lambda$lambda_3($receiver) {
  }
  function Calc_init$lambda$lambda_4(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('B: ' + this$Calc.breg);
      set_id($receiver, 'breg');
    };
  }
  function Calc_init$lambda$lambda_5($receiver) {
  }
  function Calc_init$lambda$lambda$lambda($receiver) {
    $receiver.width = '20';
    set_id($receiver, 'acc');
  }
  function Calc_init$lambda$lambda_6($receiver) {
    textInput($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda$lambda);
    $receiver.unaryPlus_pdl1vz$('Accumulator');
  }
  function Calc_init$lambda$lambda$lambda_0($receiver) {
    set_id($receiver, 'code');
    $receiver.value = '';
  }
  function Calc_init$lambda$lambda_7($receiver) {
    textInput($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda$lambda_0);
    $receiver.unaryPlus_pdl1vz$('Code');
  }
  function Calc_init$lambda$lambda$lambda_1(this$Calc) {
    return function (it) {
      this$Calc.doReset();
    };
  }
  function Calc_init$lambda$lambda_8(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('Reset');
      set_onClickFunction($receiver, Calc_init$lambda$lambda$lambda_1(this$Calc));
    };
  }
  function Calc_init$lambda$lambda$lambda_2(this$Calc) {
    return function (it) {
      this$Calc.doStep();
    };
  }
  function Calc_init$lambda$lambda_9(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('Step');
      set_onClickFunction($receiver, Calc_init$lambda$lambda$lambda_2(this$Calc));
    };
  }
  function Calc_init$lambda$lambda$lambda_3(this$Calc) {
    return function (it) {
      this$Calc.doPlus();
    };
  }
  function Calc_init$lambda$lambda_10(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(' + ');
      set_onClickFunction($receiver, Calc_init$lambda$lambda$lambda_3(this$Calc));
    };
  }
  function Calc_init$lambda$lambda$lambda_4(this$Calc) {
    return function (it) {
      this$Calc.doEq();
    };
  }
  function Calc_init$lambda$lambda_11(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(' = ');
      set_onClickFunction($receiver, Calc_init$lambda$lambda$lambda_4(this$Calc));
    };
  }
  function Calc_init$lambda$lambda$lambda_5(closure$but, this$Calc) {
    return function (it) {
      println('click ' + closure$but + ' test');
      this$Calc.doButton_za3lpa$(closure$but);
    };
  }
  function Calc_init$lambda$lambda_12(closure$but, this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$but.toString());
      set_onClickFunction($receiver, Calc_init$lambda$lambda$lambda_5(closure$but, this$Calc));
    };
  }
  function Calc_init$lambda$lambda$lambda_6(this$Calc) {
    return function (it) {
      this$Calc.backButton();
    };
  }
  function Calc_init$lambda$lambda_13(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('<');
      set_onClickFunction($receiver, Calc_init$lambda$lambda$lambda_6(this$Calc));
    };
  }
  function Calc_init$lambda$lambda$lambda_7(this$Calc) {
    return function (it) {
      this$Calc.doButton_za3lpa$(0);
    };
  }
  function Calc_init$lambda$lambda_14(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('0');
      set_onClickFunction($receiver, Calc_init$lambda$lambda$lambda_7(this$Calc));
    };
  }
  function Calc_init$lambda$lambda$lambda_8(this$Calc) {
    return function (it) {
      this$Calc.clearButton();
    };
  }
  function Calc_init$lambda$lambda_15(this$Calc) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('C');
      set_onClickFunction($receiver, Calc_init$lambda$lambda$lambda_8(this$Calc));
    };
  }
  function Calc_init$lambda(closure$data, this$Calc) {
    return function ($receiver) {
      h1($receiver, void 0, Calc_init$lambda$lambda(closure$data));
      dummyul($receiver, closure$data);
      a($receiver, 'https://kotlinfrompython.wordpress.com/2017/06/27/machine-code-and-global-memory/', void 0, void 0, Calc_init$lambda$lambda_0);
      br($receiver);
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_1);
      br($receiver);
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_2(this$Calc));
      br($receiver, void 0, Calc_init$lambda$lambda_3);
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_4(this$Calc));
      br($receiver, void 0, Calc_init$lambda$lambda_5);
      div($receiver, void 0, Calc_init$lambda$lambda_6);
      div($receiver, void 0, Calc_init$lambda$lambda_7);
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_8(this$Calc));
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_9(this$Calc));
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_10(this$Calc));
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_11(this$Calc));
      br($receiver);
      for (var but = 1; but <= 9; but++) {
        button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_12(but, this$Calc));
        if (but % 3 === 0)
          br($receiver);
      }
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_13(this$Calc));
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_14(this$Calc));
      button($receiver, void 0, void 0, void 0, void 0, Calc_init$lambda$lambda_15(this$Calc));
    };
  }
  Calc.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Calc',
    interfaces: []
  };
  function jsonloads($receiver) {
    return loads($receiver);
  }
  function main(args) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    var root = (tmp$ = document.getElementById('root')) != null ? tmp$ : Kotlin.throwNPE();
    var jsdataelt = (tmp$_0 = document.getElementById('jsondata')) == null || Kotlin.isType(tmp$_0, HTMLScriptElement) ? tmp$_0 : Kotlin.throwCCE();
    var jsdata = (tmp$_2 = (tmp$_1 = jsdataelt != null ? jsdataelt.text : null) != null ? jsonloads(tmp$_1) : null) != null ? tmp$_2 : jsonloads('');
    println('loads done');
    var jsmap = Kotlin.isType(jsdata, Map) ? jsdata : mapOf(to('none', 0));
    var pageTitle = (tmp$_3 = document.getElementsByTagName('title')[0]) == null || Kotlin.isType(tmp$_3, HTMLTitleElement) ? tmp$_3 : Kotlin.throwCCE();
    if (pageTitle != null) {
      var tmp$_4;
      pageTitle.text = Kotlin.toString((Kotlin.isType(tmp$_4 = jsmap, Kotlin.kotlin.collections.Map) ? tmp$_4 : Kotlin.throwCCE()).get_11rb$('title'));
    }
    var calc = new Calc(root, jsmap);
  }
  function iterMarks$lambda(closure$idx_0, closure$strng_0, closure$specials_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$iterMarks$lambda(closure$idx_0, closure$strng_0, closure$specials_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function Coroutine$iterMarks$lambda(closure$idx_0, closure$strng_0, closure$specials_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$idx = closure$idx_0;
    this.local$closure$strng = closure$strng_0;
    this.local$closure$specials = closure$specials_0;
    this.local$tmp$ = void 0;
    this.local$inquotes = void 0;
    this.local$found = void 0;
    this.local$index = void 0;
    this.local$$receiver = $receiver_0;
  }
  Coroutine$iterMarks$lambda.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$iterMarks$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$iterMarks$lambda.prototype.constructor = Coroutine$iterMarks$lambda;
  Coroutine$iterMarks$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            this.local$inquotes = listOf(['\\', '"']);
            this.local$index = this.local$closure$idx;
            this.state_0 = 2;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            this.local$found = findAnyOf(this.local$closure$strng, this.local$closure$specials, this.local$index);
            if (Kotlin.equals(this.local$found != null ? this.local$found.second : null, '"')) {
              this.local$index = this.local$found.first;
              inner: do {
                this.local$found = findAnyOf(this.local$closure$strng, this.local$inquotes, this.local$index + 1 | 0);
                this.local$tmp$ = this.local$found != null ? this.local$found.second : null;
                if (Kotlin.equals(this.local$tmp$, '"')) {
                  this.local$index = this.local$found.first + 1 | 0;
                  break inner;
                }
                 else if (Kotlin.equals(this.local$tmp$, '\\'))
                  this.local$index = this.local$found.first + 1 | 0;
              }
               while (this.local$found != null);
              this.state_0 = 5;
              continue;
            }
             else {
              if (this.local$found != null) {
                this.state_0 = 3;
                this.result_0 = this.local$$receiver.yield_11rb$(this.local$found, this);
                if (this.result_0 === intrinsics.COROUTINE_SUSPENDED)
                  return intrinsics.COROUTINE_SUSPENDED;
                break;
              }
               else {
                this.state_0 = 4;
                continue;
              }
            }

          case 3:
            this.local$index = this.local$found.first + 1 | 0;
            this.state_0 = 4;
            continue;
          case 4:
            this.state_0 = 5;
            continue;
          case 5:
            if (this.local$found == null) {
              this.state_0 = 6;
              continue;
            }

            this.state_0 = 2;
            continue;
          case 6:
            return;
        }
      }
       catch (e) {
        if (this.state_0 === 1)
          throw e;
        else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function iterMarks(strng, specials, idx) {
    if (idx === void 0)
      idx = 0;
    return buildSequence(iterMarks$lambda(idx, strng, specials));
  }
  function Parser(strng) {
    this.strng = strng;
  }
  Parser.prototype.whiteSpace_s8itvh$ = Kotlin.defineInlineFunction('MagicCalc.objJSON.Parser.whiteSpace_s8itvh$', function (ch) {
    return Kotlin.kotlin.text.contains_sgbm27$(' \t\n', Kotlin.unboxChar(ch));
  });
  Parser.prototype.consume_za3lpa$ = function (start) {
    var res = findAnyOf(this.strng, listOf([',', '}', ']']), start + 1 | 0);
    return Kotlin.equals(res != null ? res.second : null, ',') ? res.first + 1 | 0 : start + 1 | 0;
  };
  Parser.prototype.stripQuotes_61zpoe$ = function (fullstrng) {
    var dropWhile$result;
    dropWhile$break: do {
      var tmp$, tmp$_0, tmp$_1, tmp$_2;
      tmp$ = Kotlin.kotlin.text.get_indices_gw00vp$(fullstrng);
      tmp$_0 = tmp$.first;
      tmp$_1 = tmp$.last;
      tmp$_2 = tmp$.step;
      for (var index = tmp$_0; index <= tmp$_1; index += tmp$_2) {
        if (!Kotlin.kotlin.text.contains_sgbm27$(' \t\n', Kotlin.unboxChar(Kotlin.unboxChar(Kotlin.toBoxedChar(fullstrng.charCodeAt(index)))))) {
          dropWhile$result = fullstrng.substring(index);
          break dropWhile$break;
        }
      }
      dropWhile$result = '';
    }
     while (false);
    var strng = dropWhile$result;
    var start = Kotlin.unboxChar(strng.charCodeAt(0)) === 34 ? 1 : 0;
    var end = Kotlin.unboxChar(last(strng)) === 34 ? 2 : 1;
    return slice(strng, new IntRange(start, strng.length - end | 0));
  };
  Parser.prototype.toType_61zpoe$ = function (strng) {
    var ival = toIntOrNull(strng);
    if (ival != null) {
      return ival;
    }
    return this.stripQuotes_61zpoe$(strng);
  };
  Parser.prototype.adder_t77zsf$ = function ($receiver, adStr, obj) {
    if (obj === void 0)
      obj = null;
    var tmp$;
    var key = this.stripQuotes_61zpoe$.call(this, split_0(Kotlin.kotlin.text.trim_gw00vp$(Kotlin.isCharSequence(tmp$ = adStr) ? tmp$ : Kotlin.throwCCE()).toString(), [':']).get_za3lpa$(0));
    if (obj == null) {
      var tmp$_0;
      if (contains(adStr, ':')) {
        var tmp$_1;
        tmp$_0 = this.toType_61zpoe$.call(this, split_0(Kotlin.kotlin.text.trim_gw00vp$(Kotlin.isCharSequence(tmp$_1 = adStr) ? tmp$_1 : Kotlin.throwCCE()).toString(), [':'], void 0, 2).get_za3lpa$(1));
      }
       else
        tmp$_0 = null;
      var value = tmp$_0;
      $receiver.put_xwzc9p$(key, value);
    }
     else {
      $receiver.put_xwzc9p$(key, obj);
    }
  };
  Parser.prototype.getStart = function () {
    var count = 0;
    while (true) {
      var tmp$ = count < this.strng.length;
      if (tmp$) {
        tmp$ = Kotlin.kotlin.text.contains_sgbm27$(' \t\n', Kotlin.unboxChar(Kotlin.unboxChar(this.strng.charCodeAt(count))));
      }
      if (!tmp$)
        break;
      count = count + 1 | 0;
    }
    if (Kotlin.unboxChar(this.strng.charCodeAt(count)) === 123) {
      count = count + 1 | 0;
    }
    return count;
  };
  Parser.prototype.parse_za3lpa$ = function (idxIn) {
    if (idxIn === void 0)
      idxIn = -1;
    var tmp$, tmp$_0;
    var specials = listOf(['"', '{', '}', ',', '[']);
    var hold = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$();
    var idx = idxIn < 0 ? this.getStart() : idxIn;
    tmp$ = iterMarks(this.strng, specials, idx).iterator();
    loop: while (tmp$.hasNext()) {
      var mark = tmp$.next();
      if (mark != null)
        if (mark.first > idx) {
          tmp$_0 = mark.second;
          if (Kotlin.equals(tmp$_0, '}')) {
            this.adder_t77zsf$(hold, slice(this.strng, new IntRange(idx, mark.first - 1 | 0)));
            idx = this.consume_za3lpa$(mark.first);
            break loop;
          }
           else if (Kotlin.equals(tmp$_0, '[')) {
            var res;
            res = this.parseLst_za3lpa$(mark.first + 1 | 0);
            this.adder_t77zsf$(hold, slice(this.strng, new IntRange(idx, mark.first - 1 | 0)), res.first);
            idx = res.second;
          }
           else if (Kotlin.equals(tmp$_0, '{')) {
            var res_0;
            res_0 = this.parse_za3lpa$(mark.first + 1 | 0);
            this.adder_t77zsf$(hold, slice(this.strng, new IntRange(idx, mark.first - 1 | 0)), res_0.first);
            idx = res_0.second;
          }
           else if (Kotlin.equals(tmp$_0, ',')) {
            this.adder_t77zsf$(hold, slice(this.strng, new IntRange(idx, mark.first - 1 | 0)));
            idx = mark.first + 1 | 0;
          }
        }
    }
    return new Pair(hold, idx);
  };
  Parser.prototype.adder_ariqbg$ = function ($receiver, adStr, obj) {
    if (obj === void 0)
      obj = null;
    if (obj == null) {
      $receiver.add_11rb$(this.toType_61zpoe$(adStr));
    }
     else {
      $receiver.add_11rb$(obj);
    }
  };
  Parser.prototype.parseLst_za3lpa$ = function (idxIn) {
    if (idxIn === void 0)
      idxIn = 1;
    var tmp$, tmp$_0;
    var specials = listOf(['"', '{', ']', ',', '[']);
    var lst = Kotlin.kotlin.collections.ArrayList_init_ww73n8$();
    var idx = idxIn;
    tmp$ = iterMarks(this.strng, specials, idx).iterator();
    loop: while (tmp$.hasNext()) {
      var mark = tmp$.next();
      if (mark != null)
        if (mark.first > idx) {
          tmp$_0 = mark.second;
          if (Kotlin.equals(tmp$_0, ']')) {
            this.adder_ariqbg$(lst, slice(this.strng, new IntRange(idx, mark.first - 1 | 0)));
            idx = this.consume_za3lpa$(mark.first);
            break loop;
          }
           else if (Kotlin.equals(tmp$_0, '[')) {
            var res;
            res = this.parseLst_za3lpa$(mark.first + 1 | 0);
            this.adder_ariqbg$(lst, slice(this.strng, new IntRange(idx, mark.first - 1 | 0)), res.first);
            idx = res.second;
          }
           else if (Kotlin.equals(tmp$_0, '{')) {
            var res_0;
            res_0 = this.parse_za3lpa$(mark.first + 1 | 0);
            this.adder_ariqbg$(lst, slice(this.strng, new IntRange(idx, mark.first - 1 | 0)), res_0.first);
            idx = res_0.second;
          }
           else if (Kotlin.equals(tmp$_0, ',')) {
            this.adder_ariqbg$(lst, slice(this.strng, new IntRange(idx, mark.first - 1 | 0)));
            idx = mark.first + 1 | 0;
          }
        }
    }
    return new Pair(lst, idx);
  };
  Parser.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Parser',
    interfaces: []
  };
  function loads(arg) {
    return (new Parser(arg)).parse_za3lpa$().first;
  }
  function RawStr(str) {
    this.str = str;
  }
  RawStr.prototype.res = function () {
    return this.str;
  };
  RawStr.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'RawStr',
    interfaces: []
  };
  function Dumper(dataAll) {
    this.dataAll = dataAll;
  }
  Dumper.prototype.quoter_61zpoe$ = function (strng) {
    var res = '"' + strng + '"';
    return res;
  };
  Dumper.prototype.dump_za3rmp$ = function (data) {
    if (typeof data === 'number')
      return data.toString();
    else if (typeof data === 'string')
      return this.quoter_61zpoe$(data);
    else if (Kotlin.isType(data, Map))
      return this.dumpmap_bkhwtg$(data);
    else if (Kotlin.isType(data, List))
      return this.dumplst_9ma18$(data);
    else if (Kotlin.isType(data, RawStr))
      return data.res();
    return 'UnKnown';
  };
  Dumper.prototype.dumpmap_bkhwtg$ = function (mapData) {
    var $receiver = toList(mapData);
    var destination = Kotlin.kotlin.collections.ArrayList_init_ww73n8$(Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$($receiver, 10));
    var tmp$;
    tmp$ = $receiver.iterator();
    while (tmp$.hasNext()) {
      var item = tmp$.next();
      var key = item.first;
      var dat = item.second;
      var strdat = dat == null ? 'null' : this.dump_za3rmp$(dat);
      destination.add_11rb$('"' + Kotlin.toString(key) + '"' + ': ' + strdat);
    }
    var lstStr = destination;
    var iterator = lstStr.iterator();
    if (!iterator.hasNext()) {
      throw new Kotlin.kotlin.UnsupportedOperationException("Empty collection can't be reduced.");
    }
    var accumulator = iterator.next();
    while (iterator.hasNext()) {
      accumulator = accumulator + ', ' + iterator.next();
    }
    var redStr = accumulator;
    return '{ ' + redStr + ' }';
  };
  Dumper.prototype.dumplst_9ma18$ = function (lstData) {
    var destination = Kotlin.kotlin.collections.ArrayList_init_ww73n8$(Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$(lstData, 10));
    var tmp$;
    tmp$ = lstData.iterator();
    while (tmp$.hasNext()) {
      var item = tmp$.next();
      var dat = item;
      destination.add_11rb$(dat == null ? 'null' : this.dump_za3rmp$(dat));
    }
    var lstStr = destination;
    var iterator = lstStr.iterator();
    if (!iterator.hasNext()) {
      throw new Kotlin.kotlin.UnsupportedOperationException("Empty collection can't be reduced.");
    }
    var accumulator = iterator.next();
    while (iterator.hasNext()) {
      accumulator = accumulator + ', ' + iterator.next();
    }
    var redStr = accumulator;
    return '[ ' + redStr + ' ]';
  };
  Dumper.prototype.dumps = function () {
    return this.dump_za3rmp$(this.dataAll);
  };
  Dumper.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: 'Dumper',
    interfaces: []
  };
  function dumps(arg) {
    return (new Dumper(arg)).dumps();
  }
  _.dummyul_545ggl$ = dummyul;
  _.Calc = Calc;
  _.jsonloads_pdl1vz$ = jsonloads;
  _.main_kand9s$ = main;
  var package$objJSON = _.objJSON || (_.objJSON = {});
  package$objJSON.iterMarks_hjdhk9$ = iterMarks;
  package$objJSON.Parser = Parser;
  package$objJSON.loads_61zpoe$ = loads;
  package$objJSON.RawStr = RawStr;
  package$objJSON.Dumper = Dumper;
  package$objJSON.dumps_za3rmp$ = dumps;
  main([]);
  Kotlin.defineModule('MagicCalc', _);
  return _;
}(typeof MagicCalc === 'undefined' ? {} : MagicCalc, kotlin, this['kotlinx-html-js']);

//# sourceMappingURL=MagicCalc.js.map
